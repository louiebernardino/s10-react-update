import React, {Fragment} from 'react';
import { Table } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';
import {Link} from 'react-router-dom'


const TaskRow = ({taskAttr, index, deleteTask}) => {
const { description, _id, memberId, isCompleted} = taskAttr
  return (
  	<Fragment>
  		<tr>
          <th scope="row">{ index }</th>
          <td>{ memberId ? memberId.username : "N/A"  }</td>
          <td>{ memberId ? memberId._id : "N/A"}</td>
          <td>{ description }</td>
          <td>{isCompleted ? "Active": "Deactivated"}</td>
          <td><Link className="btn btn-info" to={`/tasks/${_id}`}><i className="fas fa-eye"></i></Link>
          <Button className="ml-1 mr-1" color="warning"><i className="fas fa-edit"></i></Button>
          <Button color="danger"><i className="fas fa-trash-alt" onClick={()=> deleteTask(_id)}></i></Button></td>
        </tr>
    </Fragment>
	);
}

export default TaskRow;

