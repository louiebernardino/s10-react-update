import React, {Fragment} from 'react';
import { Table } from 'reactstrap';
import { Container, Row, Col } from 'reactstrap';
import { Button } from 'reactstrap';


const TeamRow = ({teamAttr, index, deleteTeam}) => {
  console.log("team row", teamAttr)
  const {team, _id, isActive} = teamAttr
  let deleteBtn = ""

  if (isActive) {
    deleteBtn = (
        <Button size="sm" color="danger"><i className="fas fa-trash-alt" onClick={()=> deleteTeam(_id)}></i></Button>
      )
  }

  return (
  	<Fragment>
  		<tr>
          <th scope="row">{ index }</th>
          <td>{ teamAttr.name }</td>
          <td>adamg</td>
          <td>adamg@gmail.com</td>
          <td>Admin</td>
          <td>{isActive ? "Active": "Deactivated"}</td>
          <td><Button size="sm" color="primary"><i className="fas fa-eye"></i></Button>
          <Button size="sm" className="ml-1 mr-1" color="warning"><i className="fas fa-edit"></i></Button>
          { deleteBtn }
          </td>
        </tr>
    </Fragment>
	);
}

export default TeamRow;

