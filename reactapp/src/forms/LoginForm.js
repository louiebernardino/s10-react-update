import React, { useState, useEffect, Fragment } from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';
import { Link, Redirect } from 'react-router-dom';
import axios from 'axios';

const LoginForm = (props) => {

const [formData, setFormData] = useState({
  email: "msese@gmail.com",
  password: "12345"
})

const [disabled, setDisabled] = useState(true)

const [isRedirected, setIsRedirected] = useState(false)

const { email, password} = formData;
const onChangeHandler = e => {
  // console.log(e);
  // console.log(e.target)

  //Backticks - If we want multi-line and string interpolation
  // console.log(`e.target.name: ${e.target.name}`)
  // console.log(`e.target.value: ${e.target.value}`)

  //update the state
  setFormData({
    //spread operator - it copies whatever value
    ...formData,
    [e.target.name] : e.target.value
  })
  // console.log(formData)
}

const onSubmitHandler = async e => {
  e.preventDefault();

  const member = {
    email,
    password
  }

try {
    //create config
    const config = {
      headers: {
        'Content-Type': 'application/json'
      }
    }
    //create the body
    const body = JSON.stringify(member) //JSON newMember converting into a string because you cannot pass string in url
    //create route using axios
    const res = await axios.post("http://localhost:4051/members/login", body, config) //pass body and config
    console.log(res)
    localStorage.setItem("token", res.data.token)
    localStorage.setItem("username", res.data.member.username)

    //Redirect to login
    setIsRedirected(true)
    } catch(e) {
      localStorage.removeItem("token")
      console.log(e.response.data.message)
    }


}

//USE EFFECT
//React HOOK
//hook that tells the component what to do after render
useEffect(()=>{  
if(email !== "" && password !== ""){
  setDisabled(false)
} else {
  setDisabled(true)
}
}, [formData])

//Redirect
if(isRedirected) {
  window.location = "/profile";
}


  return (
    <Form className="m-3" onSubmit={ e=> onSubmitHandler(e)}>
      <FormGroup>
        <Label for="email">Email</Label>
        <Input type="email" name="email" id="email" value={email} onChange= {e => onChangeHandler(e)}/>
      </FormGroup>
      <FormGroup>
        <Label for="password">Password</Label>
        <Input type="password" name="password" id="password" value={password} onChange= {e => onChangeHandler(e)}/>
      </FormGroup>
      <Button color="primary" size="sm" block disabled={disabled}>Login</Button>
      <p>
        Don't have an account? <Link to="/register">Register</Link>
      </p>
    </Form>
  );

}


export default LoginForm;