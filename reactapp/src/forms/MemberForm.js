import React from 'react';
import { Button, Form, FormGroup, Label, Input, FormText } from 'reactstrap';

const MemberForm = (props) => {
  return (
    <Form className="m-3">
      <FormGroup>
        <Label for="exampleUsername">Username</Label>
        <Input type="username" name="username" id="exampleUsername"/>
      </FormGroup>
      <FormGroup>
        <Label for="exampleEmail">Email</Label>
        <Input type="email" name="email" id="exampleEmail"/>
      </FormGroup>
      <FormGroup>
        <Label for="teamSelect">Team</Label>
        <Input type="select" name="teamSelect" id="teamSelect">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
        </Input>
      </FormGroup>
      <FormGroup>
        <Label for="positionSelect">Position</Label>
        <Input type="select" name="positionSelect" id="positionSelect">
          <option>1</option>
          <option>2</option>
          <option>3</option>
          <option>4</option>
          <option>5</option>
          <option>6</option>

        </Input>
      </FormGroup>
      <Button color="primary" size="sm" block>Save Changes</Button>
    </Form>
  );
}

export default MemberForm;