import React, { useState, useEffect } from 'react'
import { Button, Modal, ModalHeader, ModalBody, Form, FormGroup, Label, Input } from 'reactstrap'

const MemberModal = (props, {modal, toggle, member}) => {
	const [memberModal, setMemberModel] = useState({
		teamId: "",
		position: "",
		teams: []
	})

	const { teamId, position, teams } = memberModal

	useEffect(() => {
		setMemberModel({
			teamId: props.member.teamId ? props.member.teamId._id : null,
			position: props.member.position ? props.member.position : null,
			teams: props.teams
		})
	}, [props.toggle])

	//POPULATE DROPDOWN
	const populateTeams = () => {
		return teams.map(team => {
			return (
				<option key ={team._id} value={team._id} selected={ teamId === team._id ? true : false}>
					{team.name}
				</option>

				)
		})
	}

	//IF TEAMID IS NULL
	let na
	if (teamId == null || position == null) {
		na = (
			<option selected disabled>Please choose one...</option>
		)
	}

	//HANDLE CHANGES TO FORM
	const onChangeHandler = (e) => {
		setMemberModel({
			...memberModal,
			[e.target.name] : e.target.value //element
		})
	}

	//HANDLE SUBMIT
	const onSubmitHandler = (e) => {
		e.preventDefault() //prevents the page from reloading
		const updates = {
			teamId,
			position
		}

		props.updateMember(props.member._id, updates)
	}

	return (
		<Modal isOpen={props.modal} toggle={props.toggle}>
			<ModalHeader toggle={props.toggle}>Update Member</ModalHeader>
			<ModalBody>
				<Form className="border rounded" onSubmit={e => onSubmitHandler(e)}>
					<FormGroup>
						<Label for="username">Username</Label>
						<Input type="text" name="username" id="username" value={props.member.username} disabled/>
					</FormGroup>
					<FormGroup>
						<Label for="teamId">Team</Label>
						<Input type="select" name="teamId" id="teamId" onChange={e => onChangeHandler(e)}>
						{ na }
						{populateTeams()}
						</Input>
					</FormGroup>
					<FormGroup>
						<Label for="position">Position</Label>
						<Input type="select" name="position" id="position" onChange={e => onChangeHandler(e)}>
							{ na }
							<option value="admin" selected={ props.member.position === "admin" ? true : false}>Admin</option>
							<option value="ca" selected={ props.member.position === "ca" ? true : false}>CA</option>
							<option value="student" selected={ props.member.position === "student" ? true : false}>Student</option>
							<option value="hr" selected={ props.member.position === "hr" ? true : false}>HR</option>
							<option value="instructor" selected={ props.member.position === "instructor" ? true : false}>Instructor</option>
						</Input>
					</FormGroup>
					<Button>Save Changes</Button>
				</Form>
			</ModalBody>
		</Modal>



		)
}

export default MemberModal